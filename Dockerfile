# lets create the actual deployment image
FROM nginx:alpine

# Copy over the site config for nginx
COPY ./.docker/default.conf /etc/nginx/conf.d/default.conf

# Copy over the built website in the "public" folder to the deployment image
COPY ./public /usr/share/nginx/html

# Change permissions to allow running as arbitrary user
RUN chmod -R 777 /var/log/nginx /var/cache/nginx /var/run \
     && chgrp -R 0 /etc/nginx \
     && chmod -R g+rwX /etc/nginx

# Use a different user as openshift wants non-root containers
# do it at the end here as it'll block our "root" commands to set the container up
USER 1000

# Expose 8081 as we cant use port 80 on openshift (non-root restriction)
EXPOSE 8081
